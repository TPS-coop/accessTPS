# RFID access to spaces and tools.

* [Tutorial Lifehacker](https://www.extremetech.com/computing/77472-how-to-create-rfid-access-for-your-front-door/2)
* [instructables](http://www.instructables.com/id/Arduino-RFID-Door-Lock/)
* [hackspace guide](https://github.com/UKHackspaceFoundation/resources/tree/develop/Hackspace%20Owners%20Manual/Infrastructure/Physical/Access%20Control)

# provided by ben
Door system links.

* https://www.quasarelectronics.co.uk/Category/kits-modules-door-entry-access-control
* https://www.doorentryonline.co.uk/acatalog/access-control-system.html
* https://github.com/leosac/leosac (intersting need a rasberry pi per system)
* http://hackaday.com/2016/04/13/carontepass-open-access-control-for-your-hackerspace/ 
* https://dallasmakerspace.org/wiki/RFID_Door_Access_Control
* http://www.pi-lock.com/

# provide by andres

## hackbarn:

* https://www.youtube.com/watch?v=CI3xxsBM9SE&feature=youtu.be
* https://www.youtube.com/watch?v=zLidqAPo3Cs
* http://koljawindeler.github.io/macs/
* https://hackaday.com/2015/12/13/maker-barn-organizer-creates-makerspace-access-control-system/

## Brighton:
* https://www.youtube.com/watch?v=cPY0XvMhg1A

## BPT

http://www.bpt.co.uk/
The IXP-220 system seems like an option but system is £1868 for a 2 reader system. If you want to add more it will be an addition £321 for an additional controller and a £83.60 for a reader. Each controller can control 2 readers. It can be retro fitted for our equipment.  Max number of readers from one main system is 256. Database is installed on a windows machine that only needs to be turned on when updating the system. 

As a not VAT registered company you cannot buy from them more than £100 but I can register with them to get discounts. 



## London Hackerspace

https://wiki.london.hackspace.org.uk/view/Project:Tool_Access_Control

server system: https://github.com/londonhackspace/acserver

* door bot: https://github.com/londonhackspace/Doorbot


## ACCX
http://www.accxproducts.com/wiki/index.php?title=Access_Control_and_Security_Wiki




# Camera and voice
All we need is: postman pushes button. Mobile phone is called. Mobile answers: Package? Please leave it out the back. 


# Other links (maybe repeated)

* https://www.quasarelectronics.co.uk/Category/kits-modules-door-entry-access-control
* http://tronixstuff.com/2013/11/19/arduino-tutorials-chapter-15-rfid/
* https://www.coolcomponents.co.uk/en/catalog/product/view/id/427/s/rfid-reader-writer-13-56mhz/category/41/
* http://wiki.seeed.cc/13.56Mhz_RFID_module-IOS-IEC_14443_type_a/
* https://www.lifewire.com/arduino-rfid-projects-2495271
* https://cpc.farnell.com/webapp/wcs/stores/servlet/ProductDisplay?catalogId=15002&langId=69&storeId=10180&krypto=IGol4W83pIUWAO3Yk0ZHyo6bELhy8o0BB0OsPASdb%2Bm6iMfI5%2Fw4Nk%2Ba57RCtgbvND9AKnLQiGla%2FC8z31UbR8GcqZpuBCUE%2FQz0PR4B4xUX%2FCHwfQEXe8sBQ45%2Btj5l8qNSPCOX7yNbBEFbZgshAUEQdgv7OL3IdyvPJ%2BoRk%2BiVYf%2FNoUCjHHMK1M8yf3LzM4x4hMZ7zljiIpQPK3sm%2F8euczzHZmcq%2FKdAiEa0Lxg%3D&ddkey=http%3Aen-CPC%2FCPC_United_Kingdom%2Fsearch
* http://www.farnell.com/datasheets/1860560.pdf
* http://www.velleman.eu/downloads/0/illustrated/illustrated_assembly_manual_k8019.pdf
* https://www.amazon.co.uk/Yosoo-ACR122U-Contactless-Reader-5xMifare/dp/B00GYPIZG6/ref=sr_1_1?ie=UTF8&qid=1502785757&sr=8-1&keywords=acr122u

Look up Southlondon Makerspace forum for details. 

* https://github.com/southlondonmakerspace/membership-kiosk
* https://github.com/londonhackspace/Doorbot/issues
* https://github.com/londonhackspace/acnode-cl
* https://wiki.london.hackspace.org.uk/view/Membership_Kiosk
* https://wiki.london.hackspace.org.uk/view/Door_control_system/Logbook
* https://wiki.london.hackspace.org.uk/view/Door_control_system
* http://www.ti.com/tool/ek-tm4c1294xl#tabs
* http://www.elechouse.com/elechouse/index.php?main_page=product_info&cPath=90_93&products_id=2242


# Jakub review of the solutions (ods attached)

Name	Link	Software	hardware	signs of life	Comments

* ForkPi	https://github.com/kieferyap/forkpi	A Raspberry Pi, ForkPi (python 3.2.3), SpoonPi (python 3.2.3)	Fingerprint scanner, RFID Scanner	last openned issue 10 days ago, last commit  Novermber 2017	
* CarontePass	https://github.com/torehc/carontepass-v2	Python Django 1.9 Dango REST framework Bootstrap with AdminLTE theme PyTelegramBotAPI ESP8266 core for Arduino	reader NFC model RC522	last issue on 25th october 2017, last commit on 1st September 2017	
* Leosac	https://github.com/leosac/leosac	A SD card with a dumped Raspbian image. GCC 4.8+ (any C++11-compatible compiler should do) Git (to clone the repo) CMake 2.8.12 (and above) Boost 1.41 (and above) TCLAP GoogleTest is required if you plan to build the test suite.	A Raspberry Pi (and a SD card). A Piface Digital board (see here). A Wiegand card reader.	last issue 11 days ago. Last commit 14 days ago.	
* RoseGuarden	https://github.com/mdrobisch/roseguarden	Raspberry Pi, Odroid, Orange Pi or BeagleBoard (based on python).	the control unit (Rapsberry Pi, Orange Pi, Beagleboard or Odroid): running the Python based app and server. a RFID-reader (e.g. RC552): reading and writing the tags. a relay-module: controling the door-openers. a dc-dc-converter: supplying the control unit with a input of e.g. 12V / 24V and a output of 5V. a micro-usb cable of 30cm length: connect dc-dc-converter to the control unit. some internal cable: e.g. from dc-dc converter to the relay module or to the raspberry pi	last issue on 20 April 2017. Last commit on 2 July 2017	
* Open access control	https://code.google.com/archive/p/open-access-control/	The latest open access control software and the latest Arduino IDE 	RFID Reader/scanner, The open-standards alternative is EM4100, you could also use RS232-based. An Arduino or compatible micro controller	Last upload October 2013	






